import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ProductProvider } from "./context/ProductContext";
import CreateProductPage from "./pages/CreateProductPage";
import ListProductPage from "./pages/ListProductPage";
import TableProductPage from "./pages/TableProductPage";
import UpdateProductPage from "./pages/UpdateProductPage";
import ProductDetail from "./pages/ProductDetail";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import ProtectedRoute from "./router/ProtectedRoute";
import GuestRoute from "./router/GuestRoute";

function App() {
  return (
    <>
    <BrowserRouter>
      <ProductProvider>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/product" element={<ListProductPage />} />
          <Route path="/detail/:id" element={<ProductDetail />} />
          <Route path="/update/:id" element={<UpdateProductPage />} />
          <Route element={<ProtectedRoute />}>
            <Route path="/table" element={<TableProductPage />} />
            <Route path="/create" element={<CreateProductPage />} />
          </Route>

          <Route element={<GuestRoute />}>
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
          </Route>
        </Routes>
      </ProductProvider>
    </BrowserRouter>
    </>
  );
}

export default App;
