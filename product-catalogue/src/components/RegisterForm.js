import axios from "axios";
import React from 'react'
import { Button, Checkbox, Form, Input } from 'antd';
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import swal from 'sweetalert';

const rulesSchema = Yup.object({
    name: Yup.string().required("Nama Pengguna wajib diisi"),
    username: Yup.string().required("Username Pengguna wajib diisi"),
    email: Yup.string()
      .required("Email Pengguna wajib diisi")
      .email("Email tidak valid"),
    password: Yup.string().required("Password wajib diisi"),
    password_confirmation: Yup.string().required(
      "Password Konfirmasi wajib diisi"
    ),
  });
  
  function RegisterForm() {
    const navigate = useNavigate();
  
    const initialState = {
      name: "",
      username: "",
      email: "",
      password: "",
      password_confirmation: "",
    };
  
    const onRegister = async (values) => {
      try {
        const response = await axios.post(
          "https://api-project.amandemy.co.id/api/register",
          {
            name: values.name,
            username: values.username,
            email: values.email,
            password: values.password,
            password_confirmation: values.password_confirmation,
          }
        );
        swal("Yeah","Berhasil Melakukan Regis","success");
        resetForm();
        // navigasi ke halaman table
        navigate("/login");
      } catch (error) {
        swal("Oops",error.response.data.info,"error");
        console.log(error);
      }
    };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onRegister,
    validationSchema: rulesSchema,
  });

  return (
    <div className="max-w-xl mx-auto border-2 border-solid border-blue-300 p-6 mt-12 content-center bg-white shadow-xl">
      <h1 className="text-center text-2xl items-center my-3">Register</h1>
      <Form
            name="basic"
            labelCol={{
            span: 8,
            }}
            wrapperCol={{
            span: 16,
            }}
            initialValues={{
            remember: true,
            }}
            
        >
            <Form.Item
            label='Name'
            className='mx-auto'
            placeholder="Masukkan Nama pengguna"
            help={touched.name === true && errors.name}
            hasFeedback={true}
            validateStatus={errors.name === true && "errors"}
            >
            <Input 
            onChange={handleChange}
            onBlur={handleBlur}
            name="name"
            value={values.name}
            />
            </Form.Item>
            
            
            <Form.Item
            label='Username'
            className='mx-auto'
            placeholder="Masukkan Username"
            help={touched.username === true && errors.username}
            hasFeedback={true}
            validateStatus={errors.username === true && "errors"}
            >
            <Input 
            onChange={handleChange}
            onBlur={handleBlur}
            name="username"
            value={values.username}
            />
            </Form.Item>
            
            <Form.Item
            label='Email'
            className='mx-auto'
            placeholder="Masukkan email pengguna"
            help={touched.email === true && errors.email}
            hasFeedback={true}
            validateStatus={errors.email === true && "errors"}
            >
            <Input 
            onChange={handleChange}
            onBlur={handleBlur}
            name="email"
            value={values.email}
            />
            </Form.Item>
            
            <Form.Item
            label="Password"
            name="password"
            placeholder="Masukkan password"
            help={touched.password === true && errors.password}
            hasFeedback={true}
            validateStatus={errors.password === true && "errors"}
            >
            <Input.Password
                onChange={handleChange}
                onBlur={handleBlur}
                name="password"
                value={values.password} />
            </Form.Item>
            
            <Form.Item
            label="Password Confirmation"
            name="password_confirmation"
            placeholder="Masukkan Konfirmasi Password"
            help={touched.password_confirmation === true && errors.password_confirmation}
            hasFeedback={true}
            validateStatus={errors.password_confirmation === true && "errors"}
            >
            <Input.Password
                onChange={handleChange}
                onBlur={handleBlur}
                name="password_confirmation"
                value={values.password_confirmation} />
            </Form.Item>

            <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
                offset: 8,
                span: 16,
            }}
            >
            <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Form.Item
            wrapperCol={{
                offset: 8,
                span: 16,
            }}
            >
            <Button type="primary" style={{ background: "blue" }} htmlType="submit" onClick={handleSubmit}>
                Submit
            </Button>
            </Form.Item>
        </Form>
    </div>
  );
}

export default RegisterForm