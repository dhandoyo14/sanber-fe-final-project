import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import axios from "axios";

const DetailProduct = () => {
    const {id} = useParams()
    const [states, setStates] = useState({
          name: "",
          harga: null,
          stock: null,
          image_url: "",
          is_diskon: false,
          harga_diskon: null,
          category: null,
          description: "",
      });
    useEffect(() => {
      console.log(`Fetch Product id ${id}`);
      fetchProductById();
    }, []);
      
  
    const fetchProductById = async () => {
      try {
        // fetch data
        const response = await axios.get(
          `https://api-project.amandemy.co.id/api/products/${id}`
        );
        const product = response.data.data;
        // melakukan binding data dari server
        setStates({
            name: product.name,
            harga: product.harga,
            stock: product.stock,
            image_url: product.image_url,
            is_diskon: product.is_diskon,
            harga_diskon: product.harga_diskon,
            category: product.category,
            description: product.description
        });
      } catch (error) {
        console.log(error);
      }
    };
  return (
    <div class="content flex justify-center mx-auto my-8">
        <div class="block flex flex-col bg-white shadow-[0_2px_15px_-3px_rgba(0,0,0,0.2),0_10px_20px_-2px_rgba(0,0,0,0.04)] dark:bg-neutral-700 min-w-full md:flex-col">
            <ul class="flex flex-row text-sm ml-4 text-blue-500 font-medium my-2">
                <Link to='/'><li>Home</li></Link>
                <p>&nbsp;/&nbsp;</p>
                <Link to='/'><li>Product</li></Link>
            </ul>
            <div class="flex flex-row">
                <img
                    class="h-96 w-full object-cover md:h-auto md:w-48 mx-1 my-1"
                    src={states.image_url}
                    alt="" />
                <div class="flex flex-col justify-start p-8">
                    <h5
                    class="mb-2 text-4xl font-bold text-neutral-800 dark:text-neutral-50">
                    {states.name}
                    </h5>
                    <p class="mb-4 text-base text-neutral-600 dark:text-neutral-200">
                    {states.description}
                    </p>
                    {states.harga_diskon&&<p class="mt-1 text-sm font-light text-lg font-bold text-red-400 line-through">
                        {states.harga_diskon.toLocaleString('en-US', {
                        style: 'currency',
                        currency: 'IDR',
                        })}
                    </p>}
                    {states.harga&&<p class="mt-1 text-lg font-medium text-gray-900">
                    {states.harga.toLocaleString('en-US', {
                        style: 'currency',
                        currency: 'IDR',
                        })}
                    </p>}
                    <p class="mt-1 text-xs font-medium text-blue-500">Stok {states.stock}</p>
                </div>
            </div>
        </div>
    </div>
  )
}

export default DetailProduct