import React from "react";
import { Link } from "react-router-dom";

function Product({ data }) {
  return (
    <Link to={`/detail/${data.id}`}>
      <div className="block">
                                
        <div className="aspect-h-1 aspect-w-1 w-full h-80 overflow-hidden rounded-lg bg-gray-200 xl:aspect-h-8 xl:aspect-w-7">
            <img src={data.image_url} alt="Sip" className="h-full w-full object-cover object-center group-hover:opacity-75"/>
        </div>
        <h3 className="mt-4 text-xl font-bold text-gray-700">{data.name}</h3>
        {data.is_diskon&&<p className="mt-1 text-sm font-light text-lg font-bold text-red-400 line-through">{data.harga_diskon_display}</p>}
        <p className="mt-1 text-lg font-medium text-gray-900">{data.harga_display}</p>
        <p className="mt-1 text-xs font-medium text-blue-500">Stock {data.stock}</p>
      </div>
    </Link>
  );
}

export default Product;
