import React  from "react";
import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import swal from 'sweetalert';



const Form = () => {
    const navigate = useNavigate();
    const [isDiskon, setDiskon] = useState(false);
    const [input, setInput] = useState({
        name: "",
        harga: null,
        stock: null,
        image_url: "",
        is_diskon: false,
        harga_diskon: null,
        category: null,
        description: "",
      });

      const rulesSchema = Yup.object().shape({
        name: Yup.string().required("Nama Produk wajib diisi"),
        harga: Yup.number().required("Harga Produk wajib diisi"),
        stock: Yup.number().required("Stok Produk wajib diisi"),
        image_url: Yup.string()
          .required("Link Gambar wajib diisi")
          .url("Link Gambar tidak valid"),
        is_diskon: Yup.bool().required("Status Diskon wajib diisi"),
        harga_diskon: Yup.number()
        .when("is_diskon", {
          is: true,
          then: () => Yup.number().required("Harga Diskon Wajib Diisi")
        }),
        category: Yup.string().required("Kategori Produk wajib diisi"),
      });

    
      const postProduct = async () => {
        console.log("Test")
        try {
          const response = await axios.post(
            "https://api-project.amandemy.co.id/api/products",
            values
          );
          swal("Yeah","Berhasil Mengirim Request","success");
          // memannggil data kembali
          resetForm();
          // navigasi ke halaman table
          navigate("/table");
        } catch (error) {
          swal("Oops",error.response.data.info,"error");
          console.log(error);
        }
      };

      const {
        handleChange,
        handleSubmit,
        errors,
        handleBlur,
        touched,
        resetForm,
        values,
      } = useFormik({
        initialValues: input,
        onSubmit: postProduct,
        validationSchema: rulesSchema,
      });
    
    return(     
        <div className="mt-0 content items-center flex justify-center">
            <div className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg content-center form-card">
                <form className="w-full max-w-sm lg:max-w-full" >
                    <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Nama Barang
                        </label>
                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-product-name"  onChange={handleChange} onBlur={handleBlur} 
          value={values.name}
 name="name" type="text" placeholder="Nama Barang"/>
                        <p className="text-red-500 text-xs italic">{touched.name === true && errors.name}</p>
                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Stok Barang
                        </label>
                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-product-stock"  onChange={handleChange} onBlur={handleBlur} 
          value={values.stock}
 name="stock" type="text" placeholder="0"/>
                    <p className="text-red-500 text-xs italic">{touched.stock === true && errors.stock}</p>
                    </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-2">
                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                        Harga
                        </label>
                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-price"  onChange={handleChange} onBlur={handleBlur} 
          value={values.harga}
 name="harga" type="text" placeholder="0"/>
                    <p className="text-red-500 text-xs italic">{touched.harga === true && errors.harga}</p>
                    </div>
                    <div className="w-full md:w-1/3 px-1 mb-6 md:mb-0 flex flex-row">
                        <input className="before:content[''] peer relative h-5 w-5 cursor-pointer appearance-none rounded-md border border-blue-gray-200 transition-all before:absolute before:top-2/4 before:left-2/4 before:block before:h-12 before:w-12 before:-translate-y-2/4 before:-translate-x-2/4 before:rounded-full before:bg-blue-gray-500 before:opacity-0 before:transition-opacity checked:border-teal-500 checked:bg-teal-500 checked:before:bg-teal-500 hover:before:opacity-10 mt-7"
                        id="grid-price"  onChange={handleChange} onBlur={handleBlur} 
          value={values.is_diskon}
 name="is_diskon" type="checkbox" placeholder="0"  onClick={()=>setDiskon(!isDiskon)}/>
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mt-6 mx-2">
                        Status Diskon
                        </label>
                        </div>
                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                        {isDiskon && <>
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Harga Diskon
                        </label>
                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-discount"  onChange={handleChange} onBlur={handleBlur} 
          value={values.harga_diskon}
 name="harga_diskon" type="text" placeholder="0"/>
                        <p className="text-red-500 text-xs italic">{touched.harga_diskon === true && errors.harga_diskon}</p>
                        </>
                        }
                    </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Jenis Barang
                        </label>
                        <div className="relative">
                            <select className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-type"  onChange={handleChange} onBlur={handleBlur} 
          value={values.category}
 name="category">
                                <option value='none'>Pilih katagori</option>
                                <option>makanan</option>
                                <option>teknologi</option>
                                <option>minuman</option>
                                <option>kendaraan</option>
                                <option>hiburan</option>
                            </select>
                            <p className="text-red-500 text-xs italic">{touched.category === true && errors.category}</p>
                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                        
                    </div>
                        <div className="w-full md:w-1/2 px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Gambar Barang
                            </label>
                            <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-image"  onChange={handleChange} onBlur={handleBlur} 
          value={values.image_url}
 name="image_url" type="text" placeholder="http://..."/>
                       <p className="text-red-500 text-xs italic">{touched.image_url === true && errors.image_url}</p>
                        </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full h-10 px-3 h-fit">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Deskripsi Barang
                            </label>
                            <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-10 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" id="grid-price"  onChange={handleChange} onBlur={handleBlur} 
          value={values.description}
 name="description" type="text" placeholder="Deskripsi"/>
                        </div>
                    </div>

                    <div className="flex flex justify-center">
                        <button className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-2 px-4 rounded"  onChange={handleChange} onBlur={handleBlur} 
 type="button" onClick={handleSubmit}>
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Form;