import axios from "axios";
import React from 'react'
import { Button, Checkbox, Form, Input } from 'antd';
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import swal from 'sweetalert';
import { Link } from "react-router-dom";


const rulesSchema = Yup.object({
    email: Yup.string()
    .required("Email Pengguna wajib diisi")
    .email("Email tidak valid"),
    password: Yup.string().required("Password wajib diisi"),
});

const LoginForm = () => {

    const navigate = useNavigate();

    const initialState = {
        email: "",
        password: "",
    };

  const onLogin = async (values) => {
    try {
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/login",
        {
          email: values.email,
          password: values.password,
        }
      );

      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("username", response.data.data.user.username);
      resetForm()
      swal("Yeah","Berhasil Melakukan Login","success");
      // navigasi ke halaman table
      navigate("/table");
    } catch (error) {
        swal("Oops",error.response.data.info,"error");
      console.log(error);
    }
  };

  const {
    handleChange,
    handleSubmit,
    errors,
    handleBlur,
    touched,
    resetForm,
    values,
    setFieldValue,
    setFieldTouched,
  } = useFormik({
    initialValues: initialState,
    onSubmit: onLogin,
    validationSchema: rulesSchema,
  });

  return (
    <div className="max-w-xl mx-auto border-2 border-solid p-6 mt-12 content-center bg-white shadow-xl">
      <h1 className="text-center text-2xl items-center my-4">Login</h1>
      <Form
            name="basic"
            labelCol={{
            span: 5,
            }}
            wrapperCol={{
            span: 15,
            }}
            initialValues={{
            remember: true,
            }}
            
        >
            <Form.Item
            label='Email'
            placeholder="Masukkan email pengguna"
            help={touched.email === true && errors.email}
            hasFeedback={true}
            validateStatus={errors.email === true && "errors"}
            >
            <Input 
            
            onChange={handleChange}
            onBlur={handleBlur}
            name="email"
            value={values.email}
            />
            </Form.Item>
            <Form.Item
            label="Password"
            name="password"
            placeholder="Masukkan password"
            help={touched.password === true && errors.password}
            hasFeedback={true}
            validateStatus={errors.password === true && "errors"}
            >
            <Input.Password
                onChange={handleChange}
                onBlur={handleBlur}
                name="password"
                value={values.password} />
            </Form.Item>

            <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
                offset: 5,
                span: 15,
            }}
            >
            <div className='flex flex-auto justify-between'>
                <Checkbox>Remember me</Checkbox>
                <Link to="/register"><h5 className='text-blue-400'>Belum punya akun?</h5></Link>
            </div>
            </Form.Item>

            <Form.Item
            wrapperCol={{
                offset: 5,
                span: 15,
            }}
            >
            <Button type="primary" style={{ background: "blue" }} htmlType="submit" onClick={handleSubmit}>
                Submit
            </Button>
            </Form.Item>
        </Form>
    </div>
  );
}

export default LoginForm