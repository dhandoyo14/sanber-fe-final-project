import axios from "axios";
import React, { useContext, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";
import { Input, Button, Dropdown } from 'antd';
import swal from 'sweetalert';

function TableProduct() {
  const { products, fetchProducts, moveToCreate, productSearch, setProductSearch } = useContext(ProductContext);
  const [ searchValue, setSearchValue] = useState('');
  useEffect(() => {
    fetchProducts();
  }, []);
  const { Search } = Input;
  const onSearch = () =>{
    setProductSearch(products.filter(product => searchValue === '' || product.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1))
  }
  const handleChange = (val) =>{
    setSearchValue(val.target.value);
  }
  const handleDropdown = (val) => {
    setProductSearch(products.filter(product => product.category === val.target.innerHTML))
  }
  
  const onDelete = async (id) => {
    try {
      // fetch data
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      swal("success","Berhasil Menghapus Product","success");
      fetchProducts();
    } catch (error) {
      console.log(error);
      swal("Ooops","Gagal Menghapus Product","error");
    }
  };
  
  const items = [
    {
      key: '1',
      label: <p onClick={handleDropdown}>makanan</p>
    },
    {
      key: '2',
      label: <p onClick={handleDropdown}>teknologi</p>
    },
    {
      key: '3',
      label: <p onClick={handleDropdown}>minuman</p>
    },
    {
      key: '4',
      label: <p onClick={handleDropdown}>kendaraan</p>
    },
    {
      key: '5',
      label: <p onClick={handleDropdown}>hiburan</p>
    },
  ];
  return (
    <section className="">
        <h1 className="my-8 text-3xl font-bold text-center">Table Product</h1>
        <div className="max-w-4xl mx-auto w-full my-4">
        
        <Dropdown
          menu={{
            items,
          }}
          placement="Pilih Kategori"
          arrow={{
            pointAtCenter: true,
          }}
          className="mr-2"
        >
          <Button >Pilih Kategori</Button>
        </Dropdown>

        <Search placeholder="input search text" onSearch={onSearch} style={{ width: 200 }} onChange={handleChange} />
        <button
          onClick={moveToCreate}
          className="px-6 py-2 bg-white text-blue-500 my-4 rounded-lg border-2 border-blue-500 float-right"
        >
          Create Product
        </button>
        <table className="border border-gray-500 w-full">
          <thead>
            <tr>
              <th className="border border-gray-500 p-2">ID</th>
              <th className="border border-gray-500 p-2">Name</th>
              <th className="border border-gray-500 p-2">Status Diskon</th>
              <th className="border border-gray-500 p-2">Harga</th>
              <th className="border border-gray-500 p-2">Harga Diskon</th>
              <th className="border border-gray-500 p-2">Image</th>
              <th className="border border-gray-500 p-2">Kategori</th>
              <th className="border border-gray-500 p-2">Action</th>
            </tr>
          </thead>
          <tbody>
            {productSearch.map((item, index) => {
              return (
                <tr>
                  <td className="border border-gray-500 p-2">{item.id}</td>
                  <td className="border border-gray-500 p-2">{item.name}</td>
                  <td className="border border-gray-500 p-2">
                    {item.is_diskon === true ? "Mati" : "Aktif"}</td>
                    
                  <td className="border border-gray-500 p-2">{item.harga.toLocaleString('en-US', {
  style: 'currency',
  currency: 'IDR',
})}</td>
                  <td className="border border-gray-500 p-2">{item.harga_diskon.toLocaleString('en-US', {
  style: 'currency',
  currency: 'IDR',
})}</td>
                  <td className="border border-gray-500 p-2">
                    <img src={item.image_url} alt="" className="w-64" />
                  </td>
                  <td className="border border-gray-500 p-2">{item.category}
                  </td>

                  <td className="border border-gray-500 p-2">
                    <div className="flex gap-2">
                      <Link to={`/update/${item.id}`}>
                        <button className="px-3 py-1 bg-yellow-600 text-white">
                          Update
                        </button>
                      </Link>
                      <button
                        onClick={() => onDelete(item.id)}
                        className="px-3 py-1 bg-red-600 text-white"
                      >
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </section>
  );
}

export default TableProduct;
