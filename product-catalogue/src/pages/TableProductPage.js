import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import TableProduct from "../components/TableProduct";
import { ProductContext } from "../context/ProductContext";
import { Helmet } from "react-helmet";

function TableProductPage() {
  const { fetchProducts } = useContext(ProductContext);
  return (
    <div>
      <Helmet>
        <title>Table Page</title>
      </Helmet>
      <Navbar />
      <TableProduct />
    </div>
  );
}

export default TableProductPage;
