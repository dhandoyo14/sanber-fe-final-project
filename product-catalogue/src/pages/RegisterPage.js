import React from 'react'
import Navbar from '../components/Navbar'
import { Helmet } from 'react-helmet'
import RegisterForm from '../components/RegisterForm'

const RegisterPage = () => {
  return (
    <div  className='block h-screen bg-blue-400'>
    <Helmet>
      <title>Register Page</title>
    </Helmet>
    <Navbar/>
    <RegisterForm />
    </div>
  )
}

export default RegisterPage