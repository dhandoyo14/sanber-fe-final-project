import React, { useContext, useEffect } from "react";
import Product from "../components/Product";
import Navbar from "../components/Navbar";
import { ProductContext } from "../context/ProductContext";
import { Helmet } from "react-helmet";
import banner from '../assets/banner.jpg'
function HomePage() {
  const { products, fetchProducts, loading, status } =
    useContext(ProductContext);

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <section>
      <Helmet>
        <title>Product List Page</title>
      </Helmet>
      <Navbar />
      <img src={banner} class="block object-fill h-1/2 rounded-lg shadow-none mx-auto"/>
      <h1 className="my-8 text-3xl font-bold text-center">List Product</h1>
      {loading === false ? (
        <div className="mx-10 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
          {products.splice(0, 4).map((item, index) => {
            return <Product data={item} />;
          })}
        </div>
      ) : (
        <h1 className="text-center my-6 text-3xl font-bold">Loading ....</h1>
      )}
    </section>
  );
}

export default HomePage;
