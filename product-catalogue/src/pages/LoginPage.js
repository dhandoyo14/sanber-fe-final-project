import React from 'react'
import LoginForm from '../components/LoginForm'
import Navbar from '../components/Navbar'
import { Helmet } from 'react-helmet'

const LoginPage = () => {
  return (
    <div className='block h-screen bg-blue-400'>
    <Helmet>
      <title>Login Page</title>
    </Helmet>
    <Navbar/>
    <LoginForm />
    </div>
  )
}

export default LoginPage