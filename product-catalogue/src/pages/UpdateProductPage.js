import React from "react";
import Navbar from "../components/Navbar";
import UpdateForm from "../components/UpdateForm";
import { Helmet } from "react-helmet";

function UpdateProductPage() {
  return (
    <div>
      <Helmet>
        <title>Update Page</title>
      </Helmet>
      <Navbar />
      <UpdateForm />
    </div>
  );
}

export default UpdateProductPage;
