import React from 'react'
import DetailProduct from '../components/DetailProduct'
import Navbar from '../components/Navbar'
import { Helmet } from 'react-helmet'

const ProductDetail = () => {
  return (
    <div>
    <Helmet>
      <title>Login Page</title>
    </Helmet>
    <Navbar/>
    <DetailProduct />
    </div>
  )
}

export default ProductDetail