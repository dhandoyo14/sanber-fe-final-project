import axios from "axios";
import { createContext, useState } from "react";
import { useNavigate } from "react-router-dom";

export const ProductContext = createContext();

export const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState([]);
  const [productSearch, setProductSearch] = useState([]);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState("success");
  const navigate = useNavigate();

  const fetchProducts = async () => {
    try {
      // fetch data
      setLoading(true); // penanda kalau kita mau mengambil data
      const response = await axios.get(
        "https://api-project.amandemy.co.id/api/products"
      );
      setProducts(response.data.data);
      setProductSearch(response.data.data);
      setStatus("success");
    } catch (error) {
      setStatus("error");
    } finally {
      // bakal selalu kejalan mau codingan errro atau ngga
      setLoading(false);
    }
  };

  const moveToCreate = () => {
    navigate("/create");
  };

  return (
    <ProductContext.Provider
      value={{
        products,
        setProducts,
        productSearch,
        setProductSearch,
        loading,
        setLoading,
        status,
        setStatus,
        fetchProducts,
        moveToCreate,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};
